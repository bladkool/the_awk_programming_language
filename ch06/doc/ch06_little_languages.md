# 6 LITTLE LANGUAGES
&nbsp;&nbsp;&nbsp;Awk is often used to develop translators for "little languages", that is, languages for specialized applications. One reason for writing a translator is to learn how a language processor works. The first example in this chapter is an assembler that in twenty lines or so shows the essentials of the assembly process. It is accompanied by an interpreter that executes the assembled programs. The combination illustrates the rudiments of assembly language and machine architecture. Other examples show the basic operation of a postfix calculator and of a recursive-descent translator for a subset of awk itself.

&nbsp;&nbsp;&nbsp;Another reason may be to experiment with the syntax or semantics of a special-purpose language before making a large investment in implementation. As examples, this chapter describes languages for drawing graphs and for specifying sort commands.

A third purpose may be to make a language for practical use, such as one of the calculators in this chapter.

Language processors are built around this conceptual model:

![alt text](./images/conceptual_model.png)

The front end, the analyzer, reads the source program and breaks it apart into its lexical units: operators, operands, and so on. It parses the source program to check that it is grammatically correct, and if it is not, issues the appropiate error messages. Finally, it translates the source program into some intermediate representation from which the back end, the synthesizer, generates the target program. The symbol table communicates information collected by the analyzer about the source program to the synthesizer, which uses it during code generation. Although we have described language processing as a sequence of clearly distinguishable phases, in practice the boundaries are often blurred and the phases may be combined.

&nbsp;&nbsp;&nbsp;Awk is useful for creating processors for experimental languages because its basic operations support many of the tasks involved in language translation. Analysis can often be handled with field splitting and regular expressions pattern matching. Symbol tables can be managed with associative arrays. Code generation can be done with printf statements.

&nbsp;&nbsp;&nbsp;In this chapter we wil develop several translators to illustrate this point or teach the lesson; embellishments and refinements are left as exercises.
	
## 	6.1 An Assembler and Interpreter
&nbsp;&nbsp;&nbsp;Our first example of a language processor is an assembler for an hypothetical computer of the sort often encountered in an introductory course on computer architecture or systems programming. The computer has a single accumulator, ten instructions, and a word-addressable memory of 1000 words. We'll assume that a "word" of machine memory holds five decimal digits; if the word is an instruction, the first two digits encode the operation and the last three digits are the address. The assembly-language instructions are shown in Table 6-1.

![alt text](./images/assembly_language_instructions.png)

&nbsp;&nbsp;&nbsp;An assembly-language program is a sequence of statements, each consisting of three fields: label, operation, and operand. Any field may be empty; labels must begin in column one. A  program may also contain comments like those in awk programs. Here is a sample assembly-language program that prints the sum of a sequence of integers; the end of the input is marked by a zero.
	
```c
# print sum of input numbers (terminated by zero)

         ld       zero    # initialize sum to zero
         st       sum
loop  get                # read a number
         jz       done   # no more input if number is zero
         add    sum    # add in accumulated sum
         st       sum    # store new value back in sum
         j         loop    # go back and read another number

done ld       sum    # print sum
         put
         halt

zero  const 0
sum  const
```
&nbsp;&nbsp;&nbsp;The target program resulting from translating this program into machine language is a sequence of integers that represents the contents of memory when the target program is ready to be run. For this program, memory looks like this:
	
```c
  0:   03010   loop     ld       zero    # initialize sum to zero
  1:   04011            st       sum
  2:   01000            loop     get     # read a number
  3:   08007            jz       done    # no more input if number is zero
  4:   05011            add      sum     # add in accumulated sum
  5:   04011            st       sum     # store new value back in sum
  6:   09002            j        loop    # go back and read another number
  7:   03011   done     ld       sum     # print sum
  8:   02000            put
  9:   10000            halt
 10:   00000   zero     const    0
 11:   00000   sum      const
```
The first field is the memory location; the second is the encoded instruction. Memory location 0 contains the translation of the first instruction of the assembly-language program, `ld zero`.

&nbsp;&nbsp;&nbsp;The assembler does its translation in two passes. Pass 1 uses field splitting to do a lexical and syntactic analysis. It reads the assembly-language program, discards comments, assigns a memory location to each label, and writes an intermediate representation of operations and operands into a temporary file. Pass 2 reads the temporary file, converts symbolic operands to the memory locations computed by pass 1, encodes the operations and operands, and outs the resulting machine-language program into the array `mem`.
As the other half of the job, we'll build an interpreter that simulates the behavior of the computer on machine-language programs. The interpreter is a loop that fetches an instruction from `mem`, decodes it into an operator and an operand, and the simulates the instruction. The program counter is kept in the variable `pc`.
	
```c
# asm - assembler and interpreter for simple computer
#   usage: awk -f asm program-file data-files...

BEGIN {
    srcfile = ARGV[1]
    ARGV[1] = ""  # remaining files are data
    tempfile = "asm.temp"
    n = split("const get put ld st add sub jpos jz j halt", x)
    for (i = 1; i <= n; i++)   # create table of op codes
        op[x[i]] = i-1

# ASSEMBLER PASS 1
    FS = "[ \t]+"
    while (getline <srcfile > 0) {
        sub(/#.*/, "")         # strip comments
        symtab[$1] = nextmem   # remember label location
        if ($2 != "") {        # save op, addr if present
            print $2 "\t" $3 >tempfile
            nextmem++
        }
    }
    close(tempfile)

# ASSEMBLER PASS 2
    nextmem = 0
    while (getline <tempfile > 0) {
        if ($2 !~ /^[0-9]*$/)  # if symbolic addr,
            $2 = symtab[$2]    # replace by numeric value
        mem[nextmem++] = 1000 * op[$1] + $2  # pack into word
    }

# INTERPRETER
    for (pc = 0; pc >= 0; ) {
        addr = mem[pc] % 1000
        code = int(mem[pc++] / 1000)
        if      (code == op["get"])  { getline acc }
        else if (code == op["put"])  { print acc }
        else if (code == op["st"])   { mem[addr] = acc }
        else if (code == op["ld"])   { acc  = mem[addr] }
        else if (code == op["add"])  { acc += mem[addr] }
        else if (code == op["sub"])  { acc -= mem[addr] }
        else if (code == op["jpos"]) { if (acc >  0) pc = addr }
        else if (code == op["jz"])   { if (acc == 0) pc = addr }
        else if (code == op["j"])    { pc = addr }
        else if (code == op["halt"]) { pc = -1 }
        else                         { pc = -1 }
    }
}
```
The associative array `symtab` records memory locations for labels. If there is no label for an input line, `symtab[""]` is set.

&nbsp;&nbsp;&nbsp;Labels start in column one, operators are preceded by white space. Pass 1 sets the field seperator variable `FS` to the regular expression `[ \t]+`. This causes every maximal sequence of blanks and tabs in the current input line to be a field seperator. In particular, leading white space is now treated as a field seperator, so `$1` is always the label and `$2` is always the operator.

Because the "op code" for `const` is zero, the single assignment
	
```c
      mem[nextmem++] = 1000 * op[$1] + $2   # pack into word
```
can be used to store both constants and instructions in pass 2.

**Exercise 6-1.** Modify `asm` to print the listing of memory and program shown above. \
**Exercise 6-2.** Augment the interpreter to print a trace of the instructions as they are executed. \
**Exercise 6-3.** To get an idea of scale, add code to handle errors, deal with a richer set of conditional jumps, etc. How would you handle literal operands like `add = 1` instead of forcing the user to create a cell called one? \
**Exercise 6-4.** Write a disassembler that converts a raw memory dump into assembly language. \
**Exercise 6-5.** Look at a real machine (e.g., the 6502, as found in Apple-II and Commodore, or the 8086 family in the IBM PC and compatibles) and try writing an assembler for a subset of its instructions. 

------------